package com.gradle.jenkins.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gradle.jenkins.model.Hero;
import com.gradle.jenkins.model.repository.HeroRepository;

@RestController
@RequestMapping("/hero")
public class HeroController {
	
	@Autowired
	HeroRepository herorepository;
	
	@GetMapping("/list")
	public List<Hero> getAllHero() {
		return this.herorepository.findAll();
	}
}
