package com.gradle.jenkins.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gradle.jenkins.model.Hero;

@Repository
public interface HeroRepository extends JpaRepository<Hero, Long> {

}
